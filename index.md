---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Full Stack: introductory project"
  text: "JWT workshop"
  actions:
    - theme: brand
      text: Get started
      link: /0.start
---

