---
outline: deep
---

# Basic authentication/authorization with JWT-tokens

In this scenario, the server issues (encoded) access
tokens to users providing valid credentials.

Requests to Web API endpoints that require authentication, must contain an 
```Authentication``` header containing the access token.
The server will decode access this access token and check whether
it's valid and what the user has access to.

## Creating and validating JWT-tokens

Let's define some reusable functions for the creation and validation of JWT-tokens.
It's a good idea to put these function in a separate class e.g. ```JWTService```.

The function ```generateJWTToken``` generates and encodes a JWT-token, given a secret key.
In this function,
**it's up to you to determine which _claims_ the token payload contains**. In the example 
below, 3 _registered claims_ are included: iat (issued at time), exp (expiration time)
and sub (subject). The latter claim (sub) is used to store the user ID (as in the database).
2 _custom claims_ are included: username and roles. The latter claim (roles) can be used for
both back end and front end authorization.

```php
// on top of this class
use Firebase\JWT\JWT;

static function generateJWTToken(int $userId, string $userName, ?array $rolesArray, string $secretKey, int $seconds = 60 * 60) : string
{
    $issuedAt = time();
    $expirationTime = $issuedAt + $seconds; // $seconds = 60 * 60 -> valid for 1 hour

    $payload = [
        'iat' => $issuedAt,
        'exp' => $expirationTime,
        'sub' => $userId,
        'username' => $userName
    ];

    if ($rolesArray !== null) {
        $payload['roles'] = $rolesArray;
    }

    return JWT::encode($payload, $secretKey, 'HS256');
}
```

The function ```validateJWTToken``` decodes a JWT-token given a secret key.
Note that this function throws an exception when the token has expired, when
the secret key doesn't match, etc.

```php
// on top of this class
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;

static function validateJWTToken(string $jwtToken, string $secretKey) : \stdClass
{
    try {
        return JWT::decode($jwtToken,  new Key($secretKey, 'HS256'));
    } catch (ExpiredException $e) {
        throw new \Exception('Token expired');
    } catch (SignatureInvalidException $e) {
        throw new \Exception('Invalid token signature');
    } catch (BeforeValidException $e) {
        throw new \Exception('Token not valid yet');
    } catch (\Exception $e) {
        throw new \Exception('Invalid token');
    }
}
```

**Source:** Sasha Bondar, Implementing JWT Authentication in PHP Applications, reintech,
https://reintech.io/blog/implementing-jwt-authentication-php-applications

## The login endpoint

Let's create a Web API endpoint that accepts the user's credentials and returns
a valid access token.

```php
// file: index.php
$router->post('/login', 'MyController@login');
```

```SECRET_KEY``` is a config parameter that contains the secret key used to encode
and decode JWT-tokens.
```ACCESS_TOKEN_LIFETIME``` holds the lifetime of an access token in seconds e.g. 60*60.

```php
// file: MyController.php
public function login() : void
{
    $bodyParams = $this->httpBody;
    $username = $bodyParams['username'] ?? false;
    $password = $bodyParams['password'] ?? false;

    if (($username !== false) && ($password !== false)) {

        // retrieve $user and $roles from database
        $user = ...
        $roles = ...

        if (($user !== false) && (password_verify($password, $user['password']))) {
            $jwtToken = JWTService::generateJWTToken($user['id'], $user['username'], $roles, SECRET_KEY, ACCESS_TOKEN_LIFETIME);
            echo json_encode(['accessToken' => $jwtToken]);
        }
        else {
            $this->message(401, 'Invalid credentials'); // 401 Unauthorized
        }

    } else {
        $this->message(400, 'Malformed request.'); // 400 Bad Request
    }
}
```

## Apply authentication and authorization to a Web API endpoint

This is preliminary example of a route handling function for an endpoint
restricted to users that have the role 'admin'.

It reads the ```Authorization``` header from the HTTP request and strips and decodes the 
access token. Note that, in case of an expired or indecodable access token, the exception
is caught, which results in a 401 response.

```php
// file: MyController.php
public function onlyAdminsAllowed() : void
{
    $headers = apache_request_headers();
    if (isset($headers['Authorization'])) {
        $jwtToken = str_ireplace('Bearer ', '', $headers['Authorization']);
        try {
            $decodedPayload = JWTService::validateJWTToken($jwtToken, SECRET_KEY);
            $userId = $decodedPayload->sub;
            $roles = $decodedPayload->roles;
            if (in_array('admin', $roles)) {
                
                // HERE COMES YOUR APPLICATION LOGIC
                
                return; // no 401
            }
        } catch (Exception $e) {
            // empty, will do 401
        }
    }
    $this->message(401, 'Invalid credentials'); // 401 Unauthorized
}
```

Of course, other variations will exist. For example, where it must be verified whether the authenticated 
user owns a requested resource, or, where authentication but no further authorization is required.

**Be smart! Don't repeat yourself!** 
* Make a separate function  for endpoint authentication.
Hint: PHP functions can have functions as arguments (type name: callable).
* Even better: transform this function to a function to a _before middleware_ that calls ```exit();``` after
building a 401 response.
