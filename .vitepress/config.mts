import {defineConfig} from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    base: '/public/vakken/full-stack-introductory-project/workshop-jwt/',
    outDir: './public',
    title: "JWT workshop",
    description: "Full Stack: introductory project - JWT workshop",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            {text: 'Home', link: '/'},
            {text: 'Workshop', link: '/0.start'}
        ],

        sidebar: [
            {text: 'Introduction', link: '/0.start'},
            {
                text: 'Back-end', items: [
                    {text: 'Start', link: '/be_01'},
                    {text: 'CORS', link: '/be_02'},
                    {text: 'Basic authentication & authorization', link: '/be_03'},
                    {text: 'Refresh tokens', link: '/be_04'},
                    {text: 'Plesk sidenote', link: '/be_05'},
                ]
            },
          {
            text: 'Front-end', items: [
              {text: 'Start', link: '/fe_01'},
                  {text: 'Project architecture', link: '/fe_02'},
                  {text: 'Login and logout', link: '/fe_03'},
                  {text: 'Role-Based Access Control', link: '/fe_04'},
                  {text: 'Authenticated API Requests', link: '/fe_05'},
                  {text: 'Handling expired access tokens', link: '/fe_06'},
                  {text: 'Conclusion', link: '/fe_07'},
            ]
          }
        ]
    }
})
