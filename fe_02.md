# Project architecture

:::info
I've moved all HTML pages to the src directory, as illustrated in the
project setup workshop and stackblitz demo.

This is not the 'default' vite MPA setup and your project architecture
may be different.
:::

```
src
│   index.html                # homepage
│
├───401
│       index.html            # 'unauthenticated' page
│
├───admin
│       index.html            # restricted: only admins
│       admin.ts
│
├───login
│       index.html            # login page
│       login.ts
│
├───logout
│       index.html            # logout page
│       logout.ts
│
├───protected
│       index.html            # restricted: only members
│       protected.ts
│
├───public
│       index.html            # available for guests
│       public.ts
│
└───ts
    │   main.ts               # global scripts on every page
    │
    ├───api
    │       product-api.ts    # all product-api services
    │
    ├───types
    │       index.ts          # our types (separate files in larger projects)
    │
    └───utils
            auth.ts           # functions related to auth
            my-fetch.ts       # custom fetch-api wrapper

 ```

In this setup:

* Every page has its own TypeScript file.
  These files are responsible for **interacting with the DOM**.
* Everything else is stored in a dedicated 'ts' directory and imported
  where needed.

This approach keeps things organized, makes it easier to
manage our code and keeps a clear separation between business logic
and presentation logic.

**For example**, this is the TypeScript file associated with the admin page:

```ts
// Filename: admin.ts

import {hasRole} from "../ts/utils/auth.ts"
import {Load} from "../ts/utils/helpers.ts"
import {getAdmin} from "../ts/api/product-api.ts"

if (!hasRole("admin")) {
    location.assign(`../401/?from=${window.location.href}`)
}

const adminContainer = document.querySelector("#admin")
if (adminContainer) {
    const load = new Load()
    load.start()
    getAdmin()
        .then((p) => (adminContainer.textContent = JSON.stringify(p, null, 2)))
        .catch((e) => (adminContainer.textContent = e + ""))
        .finally(() => load.stop())
}
```

:::info
You may notice I also have a helpers file with a 'Load' class for
handling loading animations and so on. This is not part of the
workshop.

It does make it slightly harder to just copy/paste code, so... 🏆
:::